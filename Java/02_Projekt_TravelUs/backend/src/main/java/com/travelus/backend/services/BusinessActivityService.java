package com.travelus.backend.services;

import com.travelus.backend.exceptions.IdNotFoundException;
import com.travelus.backend.models.BusinessActivity;
import com.travelus.backend.models.Destination;
import com.travelus.backend.repositories.BusinessActivityRepository;
import com.travelus.backend.repositories.DestinationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BusinessActivityService {

    /** ****************************************************************************************************************
     * Fields
     ** ****************************************************************************************************************
     */
    @Autowired
    BusinessActivityRepository businessActivityRepository;

    @Autowired
    DestinationRepository destinationRepository;

    /** ****************************************************************************************************************
     * Methods
     ** ****************************************************************************************************************
     */
    public BusinessActivity saveOrUpdateActivity(BusinessActivity activity, Long destinationId) {

        Destination destination = destinationRepository.findDestinationById(destinationId);
        if (destination == null) {
            throw new IdNotFoundException("Cannot create Activity. Destination with ID " + destinationId + " does not exist.");
        }
        System.out.println("time start: " + activity.getTimeStart());
        System.out.println("time end: " + activity.getTimeEnd());

        activity.setDestination(destination);
        destination.getBusinessActivities().add(activity);

        return businessActivityRepository.save(activity);
    }
}
