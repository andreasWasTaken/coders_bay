import React, { useState, useEffect } from "react";
import { ListGroup } from "react-bootstrap";
import BirdCard from "../components/BirdCard";

const Home = () => {
	/**
	 * Hooks
	 */
	const [birds, setBirds] = useState(null);
	const [loaded, setLoaded] = useState(false);

	useEffect(() => {
		if (birds) setLoaded(true);
	}, [birds]);

	/**
	 * Functions
	 */
	const getBirds = () => {
		fetch("http://localhost:3000/birds")
			.then((res) => res.json())
			.then((data) => setBirds(data));
	};

	/**
	 * Render
	 */
	return (
		<div className="container text-center">
			<h1 className="my-3">Endangered Birds</h1>
			<button className="btn btn-primary px-3 py-2 mb-5" onClick={getBirds}>
				Get Birds!
			</button>

			{loaded && (
				<ListGroup className="d-flex flex-row flex-wrap justify-content-center">
					{birds.map((bird) => (
						<ListGroup.Item key={bird.id} className="border-0">
							<BirdCard>{bird}</BirdCard>
						</ListGroup.Item>
					))}
				</ListGroup>
			)}
		</div>
	);
};

export default Home;
