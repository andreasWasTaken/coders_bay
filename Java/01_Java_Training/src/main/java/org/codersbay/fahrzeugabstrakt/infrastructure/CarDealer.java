package org.codersbay.fahrzeugabstrakt.infrastructure;

import java.util.HashMap;
import java.util.Iterator;

import org.codersbay.fahrzeugabstrakt.vehicle.*;

public class CarDealer implements Iterable<AbstractVehicle> {

    /**
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * Fields
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */
    private HashMap<String, AbstractVehicle> vehicles;

    private String name;
    private int numberOfParkingLots;

    /**
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * Constructor(s)
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */
    /**
     * Constructor for this CarDealer class
     * 
     * @param name                the name of this car dealership
     * @param numberOfParkingLots the number of parking lots at this car dealership
     */
    public CarDealer(String name, int numberOfParkingLots) {
        vehicles = new HashMap<>();

        setNumberOfParkingLots(numberOfParkingLots);
        setName(name);
    }

    /**
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * Methods
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */
    /**
     * Checks if there are enough employees to handle all cars at this car
     * dealership. One employee can handle up to 3 cars.
     * 
     * @param employees the number of employees at this car dealership
     * @return true if there are not enough employees, otherwise returns false
     * @throws IllegalArgumentException if employees is a negative integer
     */
    public boolean tooManyCars(int numberOfEmployees) {
        if (numberOfEmployees < 0) {
            throw new IllegalArgumentException("The number of employees of the car dealership must not be negative.");
        }
        return (numberOfEmployees * 3) < this.vehicles.size();
    }

    /**
     * Checks if there are enough parking lots for all cars at this car dealership
     * 
     * @return true if there are not enough parking lots for all cars at this car
     *         dealership, otherwise returns false
     */
    public boolean tooManyCars() {
        return this.vehicles.size() > numberOfParkingLots;
    }

    /**
     * Adds a new PKW to the HashMap of this CarDealer. Use this method if you want
     * to add PKW, wich was used as a demonstration vehicle.
     * 
     * @param type               'pkw' if the vehicle is a PKW, respectively 'lkw'
     *                           if it's a LKW
     * @param id                 A unique identification for the vehicle
     * @param brand              The brand of the vehicle
     * @param price              The price without any discounts of the vehicle
     * @param yearOfConstruction The year the vehicle was constructed
     * @param yearDemoStart      The year a vehicle of type 'pkw' started being used
     *                           for demonstration purposes. Set this to -1 if the
     *                           PKW was not used as a demonstration vehicle
     * @param yearDemoEnd        The year a vehicle of type 'pkw' ended being used
     *                           for demonstration purposes. Set this to -1 if the
     *                           PKW was not used as a demonstration vehicle
     * @return {@code true} if a PKW was able to be added to the HashMap, otherwise
     *         return {@code false}
     */
    public boolean setVehicle(String id, String brand, double price, int yearOfConstruction, int yearDemoStart,
            int yearDemoEnd) {

        boolean wasAdded = false;

        this.vehicles.put(id, new PKW(id, brand, price, yearOfConstruction, yearDemoStart, yearDemoEnd));
        wasAdded = true;

        return wasAdded;
    }

    /**
     * Adds a new PKW or LKW to the HashMap of this CarDealer. Use this method if
     * you want to add PKW, wich was not used as a demonstration vehicle.
     * 
     * @param type               'pkw' if the vehicle is a PKW, respectively 'lkw'
     *                           if it's a LKW
     * @param id                 A unique identification for the vehicle
     * @param brand              The brand of the vehicle
     * @param price              The price without any discounts of the vehicle
     * @param yearOfConstruction The year the vehicle was constructed
     * @return {@code true} if a PKW or LKW was able to be added to the HashMap,
     *         otherwise return {@code false}
     */
    public boolean setVehicle(String type, String id, String brand, double price, int yearOfConstruction) {

        boolean wasAdded = false;

        if (type.toLowerCase().equals("pkw")) {
            this.vehicles.put(id, new PKW(id, brand, price, yearOfConstruction, -1, -1));
            wasAdded = true;
        } else if (type.toLowerCase().equals("lkw")) {
            this.vehicles.put(id, new LKW(id, brand, price, yearOfConstruction));
            wasAdded = true;
        } else {
            System.out.println("Der Fahrzeugtyp muss entweder \"PKW\" oder \"LKW\" sein");
        }

        return wasAdded;
    }

    /**
     * Prints the name of this car dealership formated as following: "~ ~ ~ name ~ ~
     * ~" to the console.
     */
    public void print() {
        System.out.println("~ ~ ~ " + name + " ~ ~ ~");
    }

    /**
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * Implemented Interface Methods
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */
    /**
     * 
     */
    @Override
    public Iterator<AbstractVehicle> iterator() {
        return vehicles.values().iterator();
    }

    /**
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * Getters and Setters
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */
    /**
     * 
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @throws IllegalArgumentException if {@code name} is either an empty String or
     *                                  {@code null}
     */
    public void setName(String name) {
        if (name == "" || name == null) {
            throw new IllegalArgumentException("The name of the car dealership must not be an empty String nor Null.");
        }
        this.name = name;
    }

    /**
     * 
     */
    public HashMap<String, AbstractVehicle> getVehicles() {
        return vehicles;
    }

    /**
     * 
     */
    public AbstractVehicle getVehicle(String id) {
        return vehicles.get(id);
    }

    /**
     * 
     */
    public int getNumberOfCars() {
        return vehicles.size();
    }

    /**
     * 
     */
    public int getNumberOfParkingLots() {
        return numberOfParkingLots;
    }

    /**
     * 
     * @throws IllegalArgumentException if {@code amountParkingLots} is negative
     */
    public void setNumberOfParkingLots(int numberOfParkingLots) {
        if (numberOfParkingLots < 0) {
            throw new IllegalArgumentException(
                    "The amount of available parking lots of the car dealership must not be negative.");
        }
        this.numberOfParkingLots = numberOfParkingLots;
    }

}