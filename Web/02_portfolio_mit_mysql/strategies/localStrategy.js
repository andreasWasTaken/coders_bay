const LocalStrategy = require("passport-local").Strategy;
const db = require("../db");

const strategyOptions = { usernameField: "email" };
const strategy = new LocalStrategy(strategyOptions, async (email, password, done) => {
	try {
		const admin = await db.findAdminByCredentials(email, password);
		done(null, admin);
	} catch (error) {
		done(error);
	}
});

module.exports = strategy;
