package com.travelus.backend.controllers;

import com.travelus.backend.models.NormalUser;
import com.travelus.backend.payloads.JwtLoginSuccessResponse;
import com.travelus.backend.payloads.LoginRequest;
import com.travelus.backend.security.JwtProvider;
import com.travelus.backend.services.NormalUserService;
import com.travelus.backend.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.travelus.backend.security.SecurityConstants.TOKEN_PREFIX;

@RestController
@RequestMapping("/api/user")
@CrossOrigin
public class NormalUserController {

    /** ****************************************************************************************************************
     * Fields
     ** ****************************************************************************************************************
     */
    @Autowired
    private NormalUserService normalUserService;

    @Autowired
    private UserValidator userValidator;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtProvider jwtProvider;

    /** ****************************************************************************************************************
     * Methods
     ** ****************************************************************************************************************
     */
    @PostMapping("/register")
    public ResponseEntity<?> createUser(@Valid @RequestBody NormalUser user, BindingResult result) {

        userValidator.validate(user, result);

        if (result.hasFieldErrors()) {
            return new ResponseEntity<>(result.getFieldErrors(), HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(normalUserService.saveNormalUser(user), HttpStatus.CREATED);
    }

    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest, BindingResult result) {

        if (result.hasFieldErrors()) {
            return new ResponseEntity<>(result.getFieldErrors(), HttpStatus.BAD_REQUEST);
        }

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsername(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = TOKEN_PREFIX + jwtProvider.generateToken(authentication);

        return new ResponseEntity<>(new JwtLoginSuccessResponse(true, jwt), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getUserById(@PathVariable Long id) {

        return new ResponseEntity<>(normalUserService.findUserById(id), HttpStatus.OK);
    }

    @GetMapping("/byEmail/{username}")
    public ResponseEntity<?> getUserByEmail(@PathVariable String username) {

        return new ResponseEntity<>(normalUserService.findNormalUserByUsername(username), HttpStatus.OK);
    }

    @GetMapping("/all")
    public ResponseEntity<?> getAllUsers() {

        return new ResponseEntity<>(normalUserService.findAllUsers(), HttpStatus.OK);
    }

    @GetMapping("/normal")
    public Iterable<NormalUser> getAllNormalUsers() {

        return normalUserService.findAllNormalUsers();
    }

    @GetMapping("/premium")
    public Iterable<NormalUser> getAllPremiumUsers() {

        return normalUserService.findAllPremiumUsers();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUserById(@PathVariable Long id) throws Exception {

        normalUserService.deleteUserById(id);

        return new ResponseEntity<>("User with id '" + id + "' successfully deleted", HttpStatus.OK);
    }
}
