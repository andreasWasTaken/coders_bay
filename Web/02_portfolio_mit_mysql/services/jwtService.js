const jwt = require("jsonwebtoken");

const JWT_SECRET = process.env.JWT_SECRET;
const JWT_EXPIRES_IN = "1d";

function signAdmin(admin) {
	return jwt.sign(
		{
			sub: admin.id,
			name: admin.firstName + " " + admin.lastName,
			email: admin.email
		},
		JWT_SECRET,
		{ expiresIn: JWT_EXPIRES_IN }
	);
}

module.exports = {
	signAdmin,
	JWT_SECRET
};
