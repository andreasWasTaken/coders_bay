package com.travelus.backend.services;

import com.travelus.backend.exceptions.IdNotFoundException;
import com.travelus.backend.models.NormalUser;
import com.travelus.backend.models.Trip;
import com.travelus.backend.repositories.DestinationRepository;
import com.travelus.backend.repositories.TripRepository;
import com.travelus.backend.repositories.NormalUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TripService {

    /** ****************************************************************************************************************
     * Fields
     ** ****************************************************************************************************************
     */
    @Autowired
    TripRepository tripRepository;

    @Autowired
    NormalUserRepository normalUserRepository;

    @Autowired
    DestinationRepository destinationRepository;

    /** ****************************************************************************************************************
     * Methods
     ** ****************************************************************************************************************
     */
    public Trip saveTripAndAddtoUser(Trip trip, Long userId) {

        NormalUser user = normalUserRepository.findNormalUserById(userId);
        if (user == null) {
            throw new IdNotFoundException("Cannot create trip. User with ID " + userId + " does not exist.");
        }

        trip.setUser(user);
        user.getTrips().add(trip);

        return tripRepository.save(trip);
    }

    public Trip findTripById(Long id) {

        return tripRepository.findTripById(id);
    }

    public Iterable<Trip> findAllTrips() {

        return tripRepository.findAll();
    }

    public Iterable<Trip> findAllTripsByUserId(Long id) {

        if (normalUserRepository.findNormalUserById(id) == null) {
            throw new IdNotFoundException("Cannot find trips of user with ID " + id + ". This user does not exist");
        }

        return tripRepository.findAllTripsByUserId(id);
    }

    public void deleteTripById(Long id) {

        if (tripRepository.findTripById(id) == null) {
            throw new IdNotFoundException("Cannot delete trip with ID " + id + ". This trip does not exist");
        }

        Trip trip = tripRepository.findTripById(id);
        NormalUser user = normalUserRepository.findNormalUserById(trip.getUser().getId());

        /* BUG #1 fix */
        /*while(user.getTrips().contains(trip)) {
            user.getTrips().remove(trip);
        }*/

        user.getTrips().remove(trip);
        tripRepository.delete(trip);

        if (tripRepository.findTripById(id) != null) {
            throw new IdNotFoundException("Cannot delete trip with ID " + id);
        }
    }
}
