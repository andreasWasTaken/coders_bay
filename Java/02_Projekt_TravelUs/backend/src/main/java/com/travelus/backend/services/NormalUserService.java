package com.travelus.backend.services;

import com.travelus.backend.exceptions.UsernameAlreadyExistsException;
import com.travelus.backend.exceptions.UsernameNotFoundException;
import com.travelus.backend.exceptions.IdNotFoundException;
import com.travelus.backend.models.NormalUser;
import com.travelus.backend.repositories.NormalUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class NormalUserService {

    /** ****************************************************************************************************************
     * Fields
     ** ****************************************************************************************************************
     */
    @Autowired
    private NormalUserRepository normalUserRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    /** ****************************************************************************************************************
     * Methods
     ** ****************************************************************************************************************
     */
    public NormalUser saveNormalUser(NormalUser user) {

        try {
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            user.setUsername(user.getUsername());

            if (user.getPremium() == null) {
                user.setPremium(false);
            }

            user.setConfirmPassword("");
            return normalUserRepository.save(user);
        } catch(Exception e) {
            throw new UsernameAlreadyExistsException("Username '" + user.getUsername() + "' already exists");
        }
    }

    public void deleteUserById(Long id) {

        if (normalUserRepository.findNormalUserById(id) == null) {
            throw new IdNotFoundException("Cannot delete user with ID "
                    + id + ". This user does not exist.");
        }

        normalUserRepository.deleteById(id);
    }

    public NormalUser findUserById(Long id) {

        if (normalUserRepository.findNormalUserById(id) == null) {
            throw new IdNotFoundException("Cannot find user with ID " + id + ". This user does not exist.");
        }

        return normalUserRepository.findNormalUserById(id);
    }

    public NormalUser findNormalUserByUsername(String username) {

        NormalUser user = normalUserRepository.findNormalUserByUsername(username);

        if (user == null) {
            throw new UsernameNotFoundException("Cannot find user with username '" + username + "'. This user does not exist");
        }

        return user;
    }

    public Iterable<NormalUser> findAllUsers() {

        return normalUserRepository.findAll();
    }

    public Iterable<NormalUser> findAllNormalUsers() {

        return normalUserRepository.findAllByPremiumFalse();
    }

    public Iterable<NormalUser> findAllPremiumUsers() {

        return normalUserRepository.findAllByPremiumTrue();
    }

}
