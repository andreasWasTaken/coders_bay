package com.travelus.backend.controllers;

import com.travelus.backend.models.CasualActivity;
import com.travelus.backend.services.CasualActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/activity")
public class CasualActivityController {

    /** ****************************************************************************************************************
     * Fields
     ** ****************************************************************************************************************
     */
    @Autowired
    CasualActivityService casualActivityService;

    /** ****************************************************************************************************************
     * Methods
     ** ****************************************************************************************************************
     */
    @PostMapping("/{destinationId}")
    public ResponseEntity<?> createActivityAndAddToDestination(@PathVariable Long destinationId, @Valid @RequestBody CasualActivity activity,
                                                               BindingResult result) {

        if (result.hasFieldErrors()) {

            return new ResponseEntity<>(result.getFieldErrors(), HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(casualActivityService.saveOrUpdateActivity(activity, destinationId), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteActivityById(@PathVariable Long id) {

        casualActivityService.deleteActivityById(id);

        return new ResponseEntity<>("Activity with ID " + id + " successfully deleted", HttpStatus.OK);
    }

    @GetMapping("/all/{destinationId}")
    public Iterable<CasualActivity> getAllActivitiesByDestinationId(@PathVariable Long destinationId) {

        return casualActivityService.findAllActivitiesByDestinationId(destinationId);
    }

}
