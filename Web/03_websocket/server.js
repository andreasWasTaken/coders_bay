const express = require("express");
const path = require("path");
const WebSocket = require("ws");

const PORT = 3000;
const HOST_NAME = "10.217.50.66";

// Initialize app
const app = express();

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

const wss = new WebSocket.Server({ host: "10.217.50.66", port: 8080 });

wss.on("connection", (ws) => {
	ws.room = "";
	ws.on("message", (message) => {
		// websocketSendToAll(message);
		console.log(`Received message => ${message}`);
		let msg = JSON.parse(message);

		if (msg.joinRoom) {
			ws.room = msg.joinRoom;
		}
		if (msg.room) {
			websocketSendToAll(message);
		}
	});
	ws.send(JSON.stringify({ message: "Hello! Message From Server!!" }));
});

function websocketSendToAll(text) {
	wss.clients.forEach(function each(client) {
		if (client.readyState === WebSocket.OPEN) {
			if (client.room === JSON.parse(text).room) {
				client.send(text);
			}
		}
	});
}

// Routes
app.get("/", (req, res) => {
	res.render("chat");
});

// Listen to port
app.listen(PORT, () => console.log(`Server running on http://${HOST_NAME}:${PORT}`));
