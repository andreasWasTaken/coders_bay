package com.travelus.backend.security;

public class SecurityConstants {

    public static final String URLS_SIGN_UP = "/api/user/**";
    public static final String URL_H2 = "h2-console/**";
    public static final String SECRET = "0fe6ff3972bf30618368c1fbe0abed13e0cc9dc0118640b19e742dbc1c087be7";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final long EXPIRATION_TIME = 30_000;  // 30 seconds
}
