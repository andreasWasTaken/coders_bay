package com.travelus.backend.services;

import com.travelus.backend.exceptions.IdNotFoundException;
import com.travelus.backend.models.CasualActivity;
import com.travelus.backend.models.Destination;
import com.travelus.backend.repositories.CasualActivityRepository;
import com.travelus.backend.repositories.DestinationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CasualActivityService {

    /** ****************************************************************************************************************
     * Fields
     ** ****************************************************************************************************************
     */
    @Autowired
    CasualActivityRepository casualActivityRepository;

    @Autowired
    DestinationRepository destinationRepository;

    /** ****************************************************************************************************************
     * Methods
     ** ****************************************************************************************************************
     */
    public CasualActivity saveOrUpdateActivity(CasualActivity activity, Long destinationId) {

        Destination destination = destinationRepository.findDestinationById(destinationId);
        if (destination == null) {
            throw new IdNotFoundException("Cannot create Activity. Destination with ID " + destinationId + " does not exist.");
        }

        activity.setDestination(destination);
        destination.getCasualActivities().add(activity);

        return casualActivityRepository.save(activity);
    }

    public void deleteActivityById(Long id) {

        CasualActivity activity = casualActivityRepository.findCasualActivityById(id);

        if (activity == null) {
            throw new IdNotFoundException("Cannot delete activity with ID " + id + ". Activity does not exist.");
        }


        Destination destination = destinationRepository.findDestinationById(activity.getDestination().getId());

        destination.getCasualActivities().remove(activity);
        casualActivityRepository.delete(casualActivityRepository.findCasualActivityById(id));

        if (casualActivityRepository.findCasualActivityById(id) != null) {
            throw new IdNotFoundException("Cannot delete activity with ID " + id);
        }
    }

    public Iterable<CasualActivity> findAllActivitiesByDestinationId(Long destinationId) {

        if (destinationRepository.findDestinationById(destinationId) == null) {
            throw new IdNotFoundException("Cannot find activities for destination with ID " + destinationId + ". This destination does not exist");
        }

        return casualActivityRepository.findAllByDestinationId(destinationId);
    }
}
