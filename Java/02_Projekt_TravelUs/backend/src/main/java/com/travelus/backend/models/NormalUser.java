package com.travelus.backend.models;

import javax.persistence.Entity;

@Entity
public class NormalUser extends AbstractUser {

    public NormalUser() {
        super();
    }
}
