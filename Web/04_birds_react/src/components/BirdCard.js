import React, { useState, useEffect } from "react";
import { Card } from "react-bootstrap";

const BirdCard = ({ children }) => {
	/**
	 * Hooks
	 */
	const [authors, setAuthors] = useState("ddd");

	useEffect(() => {
		let formatedAuthors = "";
		let count = 0;

		children.attributes.forEach((attribute) => {
			formatedAuthors += attribute.author;

			if (count === children.attributes.length - 1) {
				setAuthors(formatedAuthors);
				return;
			} else {
				formatedAuthors += ", ";
			}

			count += 1;
		});
	}, []);

	/**
	 * Render
	 */
	return (
		<Card style={{ width: "18rem", height: "400px" }}>
			<Card.Img variant="top" src={children.pic} alt="bird" style={{ height: "200px", objectFit: "cover" }} />
			<Card.Body style={{ display: "flex", flexDirection: "column", justifyContent: "space-between" }}>
				<Card.Title className="fw-bold">{children.commonName}</Card.Title>
				<Card.Text>ID: {children.id}</Card.Text>
				<Card.Text>Found by: {authors}</Card.Text>
				<Card.Text>Status: {children.commonwealthStatus}</Card.Text>
			</Card.Body>
		</Card>
	);
};

export default BirdCard;
