package com.travelus.backend.payloads;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class LoginRequest {

    /** ****************************************************************************************************************
     * Fields
     ** ****************************************************************************************************************
     */
    @NotBlank(message = "Username must not be an empty string")
    @NotNull(message = "Username must not be null")
    private String username;

    @NotBlank(message = "Password must not be an empty string")
    @NotNull(message = "Password must not be null")
    private String password;

    /** ****************************************************************************************************************
     * Getters and Setters
     ** ****************************************************************************************************************
     */
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
