const express = require("express");
const path = require("path");
const dotenv = require("dotenv").config();
const cors = require("cors");
const passport = require("passport");
const LocalStrategy = require("./strategies/localStrategy");
const JwtStrategy = require("./strategies/jwtStrategy");

const PORT = process.env.PORT || 3000;
const HOST_NAME = process.env.HOST_NAME;

// Initialize app
const app = express();

// Apply Middelware
app.use(express.static(path.join(__dirname, "/public")));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());
app.use(passport.initialize());

passport.use("local", LocalStrategy);
passport.use("jwt", JwtStrategy);

// Routes
app.use("/api", require("./routes/api"));
app.use("/api/admin", require("./routes/api/admin"));
app.use("/auth", require("./routes/auth"));
app.use("/mail", require("./routes/mail"));

// Listen to port
app.listen(PORT, () => console.log(`Server running on http://${HOST_NAME}:${PORT}`));
