package com.travelus.backend.models;

import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class CasualActivity extends AbstractActivity {

    /** ****************************************************************************************************************
     * Fields
     ** ****************************************************************************************************************
     */
    @NotBlank(message = "Attribute dayTime must not be an empty string")
    @NotNull(message = "Attribute dayTime must not be null")
    private String dayTime; // change to enum later

    /** ****************************************************************************************************************
     * Constructor
     ** ****************************************************************************************************************
     */
    public CasualActivity() {
        super();
    }

    /** ****************************************************************************************************************
     * Getters and Setters
     ** ****************************************************************************************************************
     */
    public String getDayTime() {
        return dayTime;
    }

    public void setDayTime(String dayTime) {
        this.dayTime = dayTime;
    }
}
