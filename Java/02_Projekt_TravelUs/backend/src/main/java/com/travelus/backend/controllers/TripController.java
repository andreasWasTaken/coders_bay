package com.travelus.backend.controllers;

import com.travelus.backend.models.Trip;
import com.travelus.backend.services.TripService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping("/api/trip")
public class TripController {

    /** ****************************************************************************************************************
     * Fields
     ** ****************************************************************************************************************
     */
    @Autowired
    TripService tripService;

    /** ****************************************************************************************************************
     * Methods
     ** ****************************************************************************************************************
     */
    @PostMapping("/{userId}")
    public ResponseEntity<?> createTripAndAddToUser(@PathVariable Long userId, @Valid @RequestBody Trip trip,
                                                    BindingResult result) {

        if (result.hasFieldErrors()) {
            return new ResponseEntity<>(result.getFieldErrors(), HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(tripService.saveTripAndAddtoUser(trip, userId), HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getTripById(@PathVariable Long id) {

        return new ResponseEntity<>(tripService.findTripById(id), HttpStatus.OK);
    }

    @GetMapping("/all")
    public Iterable<Trip> getAllTrips() {

        return tripService.findAllTrips();
    }

    @GetMapping("/all/{userId}")
    public Iterable<Trip> getAllTripsByUserId(@PathVariable Long userId) {

        return tripService.findAllTripsByUserId(userId);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteTripById(@PathVariable Long id) {

        tripService.deleteTripById(id);

        return new ResponseEntity<>("Trip with ID " + id + " successfully deleted", HttpStatus.OK);
    }

}
