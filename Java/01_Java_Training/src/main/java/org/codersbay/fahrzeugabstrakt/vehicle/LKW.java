package org.codersbay.fahrzeugabstrakt.vehicle;

import java.util.Calendar;

public class LKW extends AbstractVehicle {

    /**
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * Constructor(s)
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */

    /**
     * Main constructor for this LKW
     * 
     * @param id
     * @param brand
     * @param price
     * @param yearOfConstruction
     */
    public LKW(String id, String brand, double price, int yearOfConstruction) {
        super(id, brand, price, yearOfConstruction);
    }

    /**
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * Methods
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */
    /**
     * Calculates the discount of this LKW depending on the
     * {@code yearOfConstruction} of this LKW
     * 
     * @return The calculated discount for this LKW. The highest possible discount
     *         for a LKW is 15%.
     */
    @Override
    public double getDiscount() {
        Calendar c = Calendar.getInstance();
        int years = c.get(Calendar.YEAR) - yearOfConstruction;
        double discount;

        discount = years * 0.06;
        if (discount > 0.15) {
            discount = 0.15;
        }
        return discount;
    }

    /**
     * 
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        return sb.append("LKW ").append("(").append(this.id).append(") ").append(this.brand).append(", Baujahr: ")
                .append(this.yearOfConstruction).toString();
    }

}
