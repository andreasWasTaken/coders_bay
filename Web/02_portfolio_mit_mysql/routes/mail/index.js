const express = require("express");
const passport = require("passport");
const sendMailToUser = require("../../services/mailService");

const router = express.Router();

//router.use(passport.authenticate("jwt", { session: false }));

router.post("/", (req, res) => {
	const { to, subject, text } = req.body;
	const response = sendMailToUser(to, subject, text);
	res.header("Content-Type", "application/json");
	res.json(response);
});

module.exports = router;
