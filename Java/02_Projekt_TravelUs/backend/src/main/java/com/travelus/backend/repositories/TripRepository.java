package com.travelus.backend.repositories;

import com.travelus.backend.models.Trip;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TripRepository extends CrudRepository<Trip, Long> {

    Trip findTripById(Long id);

    @Override
    Iterable<Trip> findAll();

    @Query("SELECT t FROM Trip t WHERE t.user.id = ?1")
    Iterable<Trip> findAllTripsByUserId(Long userId);
}
