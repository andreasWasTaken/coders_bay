const express = require("express");
const db = require("../../../db");
const passport = require("passport");
const jwtService = require("../../../services/jwtService");

const router = new express.Router();

/**
 * Create new admin
 */
router.post("/", async (req, res) => {
	if (!req.body.firstName || !req.body.lastName || !req.body.email || !req.body.password) {
		res.status(400).json({ message: "firstName, lastName, email or password is missing" });
		return;
	}
	try {
		const result = await db.createAdmin(req.body);
		res.json(result);
	} catch (e) {
		res.status(401).json({ message: e.message });
	}
});

/**
 * Get all admins
 */
router.get("/", async (req, res) => {
	const result = await db.getAllAdmins();
	res.json(result);
});

/**
 * Get admin by id
 */
router.get("/:id", async (req, res) => {
	const rawResult = await db.getAdminById(req.params.id);

	if (rawResult.length == 0) {
		res.status(401).send("Admin with id '" + req.params.id + "' does not exist");
		return;
	}

	const result = {
		id: rawResult[0].id,
		firstName: rawResult[0].firstName,
		lastName: rawResult[0].lastName,
		email: rawResult[0].email,
		createdAt: rawResult[0].createdAt
	};

	res.json(result);
});

/**
 * Get admin by email
 */
router.get("/byEmail/:email", async (req, res) => {
	const rawResult = await db.getAdminByEmail(req.params.email);

	if (rawResult.length == 0) {
		res.status(401).send("Admin with email '" + req.params.email + "' does not exist");
		return;
	}

	const result = {
		id: rawResult[0].id,
		firstName: rawResult[0].firstName,
		lastName: rawResult[0].lastName,
		email: rawResult[0].email,
		createdAt: rawResult[0].createdAt
	};

	res.json(result);
});

/**
 * Delete admin by id
 */
router.delete("/:id", async (req, res) => {
	const result = await db.deleteAdminById(req.params.id);
	res.json(result);
});

/**
 * Delete admin by email
 */
router.delete("/byEmail/:email", async (req, res) => {
	try {
		const result = await db.deleteAdminByEmail(req.params.email);
		res.json(result);
	} catch (e) {
		res.status(401).json({ message: e.message });
	}
});

/**
 * Login admin
 */
router.post("/login", passport.authenticate("local", { session: false }), async (req, res) => {
	const jwt = jwtService.signAdmin(req.user);
	return res.send(jwt);
});

/**
 * Does email already exist
 */
router.get("/exist/:email", async (req, res) => {
	const result = await db.doesEmailExist(req.params.email);
	res.json({ emailExists: result });
});

module.exports = router;
