package org.codersbay.fahrzeugabstrakt.vehicle;

import java.util.Calendar;

public abstract class AbstractVehicle {

    protected String id;
    protected String brand;
    protected double price;
    protected int yearOfConstruction;

    /**
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * Constructor(s)
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */
    /**
     * Main constructor for this AbstractVehicle
     * 
     * @param id                 the unique identification for this PKW
     * @param brand              the brand of this PKW
     * @param price              the original price of this PKW
     * @param yearOfConstruction the year this PKW was constructed
     */
    public AbstractVehicle(String id, String brand, double price, int yearOfConstruction) {
        setId(id);
        setBrand(brand);
        setPrice(price);
        setYearOfConstruction(yearOfConstruction);
    }

    /**
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * Methods
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */
    /**
     * Calculates and gets the discounted price of this Vehicle. Depending on the
     * type of this vehicle the highest possible discounted value is 10% for PKWs
     * and 15% for LKWs off the original price.
     * 
     * @return The calculated discount of this Vehicle.
     */
    public double getDiscountedPrice() {
        return price * (1 - getDiscount());
    }

    /**
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * Abstract Methods
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */
    /**
     * Calculates and returns the discount for this Vehicle depending on the type of
     * the Vehicle and the year this Vehicle was constructed.
     * <p>
     * A LKW gets a discount of 6% for each year since it was constructed. The
     * highest possible discount for a LKW is 15% off the original price.
     * <p>
     * A PKW gets a discount of 5% for each year since it was constructed, plus an
     * additional 3% for each year it was used for demonstration purposes.
     * 
     * @return {@code 0 <= discount < 1}
     */
    public abstract double getDiscount();

    /**
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * Getters and Setters
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */
    /**
     * 
     */
    public String getId() {
        return id;
    }

    /**
     * @throws IllegalArgumentException if {@code id} is either an empty String or
     *                                  {@code null}
     */
    public void setId(String id) {
        if (id.equals("") || id == null) {
            throw new IllegalArgumentException("The id must not be either an empty String nor null");
        }
        this.id = id;
    }

    /**
     * 
     */
    public String getBrand() {
        return brand;
    }

    /**
     * @throws IllegalArgumentException if {@code brand} is either an empty String
     *                                  or {@code null}
     */
    public void setBrand(String brand) {
        if (brand.equals("") || brand == null) {
            throw new IllegalArgumentException("The brand must not be either an empty String nor null");
        }
        this.brand = brand;
    }

    /**
     * 
     */
    public double getPrice() {
        return price;
    }

    /**
     * @throws IllegalArgumentException if {@code price} is negative
     */
    public void setPrice(double price) {
        if (price < 0) {
            throw new IllegalArgumentException("The price must be greater than 0");
        }
        this.price = price;
    }

    /**
     * 
     */
    public int getYearOfConstruction() {
        return yearOfConstruction;
    }

    /**
     * @throws IllegalArgumentException if {@code yearOfConstruction} is before 1900
     *                                  or past the current year
     */
    public void setYearOfConstruction(int yearOfConstruction) {
        Calendar c = Calendar.getInstance();

        if (yearOfConstruction < 1900) {
            throw new IllegalArgumentException("The Vehicle must be constructed in a year past 1900");
        }
        if (yearOfConstruction > c.get(Calendar.YEAR)) {
            throw new IllegalArgumentException("The Vehicle must not be constructed in a year past the current year");
        }
        this.yearOfConstruction = yearOfConstruction;

    }

}
