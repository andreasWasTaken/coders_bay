package com.travelus.backend.repositories;

import com.travelus.backend.models.NormalUser;
import org.junit.Ignore;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class NormalUserRepositoryTest {

    /** ****************************************************************************************************************
     * Fields
     ** ****************************************************************************************************************
     */
    @Autowired
    private NormalUserRepository normalUserRepository;

    private NormalUser user;


    /** ****************************************************************************************************************
     * Before / After
     ** ****************************************************************************************************************
     */
    @BeforeEach
    void setUpEach() {
        normalUserRepository.deleteAll();

        if (normalUserRepository.findNormalUserByUsername("test@mail.com") == null) {
            user = new NormalUser();
            user.setFirstName("Test_FirstName");
            user.setLastName("Test_LastName");
            user.setUsername("test@mail.com");
            user.setPassword("Test_Password");
            user.setConfirmPassword("Test_Password");
        }

        if (normalUserRepository.findNormalUserByUsername("existing@mail.com") == null) {
            NormalUser existingUser = new NormalUser();
            existingUser.setFirstName("Existing_FirstName");
            existingUser.setLastName("Existing_LastName");
            existingUser.setUsername("existing@mail.com");
            existingUser.setPassword("Existing_Password");
            existingUser.setConfirmPassword("Existing_Password");
            normalUserRepository.save(existingUser);
        }
    }

    @AfterEach
    void tearDown() {}

    /** ****************************************************************************************************************
     * Tests
     ** ****************************************************************************************************************
     */
    @Test
    public void findExistingNormalUserByUsername() {

        NormalUser foundExistingUser = normalUserRepository.findNormalUserByUsername("existing@mail.com");

        assertNotNull(foundExistingUser);
        assertEquals("Existing_FirstName", foundExistingUser.getFirstName());
        assertEquals("Existing_LastName", foundExistingUser.getLastName());
        assertEquals("existing@mail.com", foundExistingUser.getUsername());
    }

    @Test
    public void saveValidUserWhoDoesNotExistInDatabase() {

        NormalUser userResult = normalUserRepository.save(user);

        assertNotNull(userResult);
        assertEquals("Test_FirstName", userResult.getFirstName());
        assertEquals("Test_LastName", userResult.getLastName());
        assertEquals("test@mail.com", userResult.getUsername());
    }

    @Test
    public void saveValidUserWhoDoesExistInDatabase() {
        normalUserRepository.save(user);
        normalUserRepository.save(user);

    }

    @Test
    public void saveUserWithInvalidFirstName() {

        user.setFirstName("");
        assertThrows(javax.validation.ConstraintViolationException.class, new Executable() {

            @Override
            public void execute() throws Throwable {
                normalUserRepository.save(user);
            }
        });

        user.setFirstName(null);
        assertThrows(javax.validation.ConstraintViolationException.class, new Executable() {

            @Override
            public void execute() throws Throwable {
                normalUserRepository.save(user);
            }
        });
    }

    @Test
    public void saveUserWithInvalidLastName() {

        user.setLastName("");
        assertThrows(javax.validation.ConstraintViolationException.class, new Executable() {

            @Override
            public void execute() throws Throwable {
                normalUserRepository.save(user);
            }
        });

        user.setLastName(null);
        assertThrows(javax.validation.ConstraintViolationException.class, new Executable() {

            @Override
            public void execute() throws Throwable {
                normalUserRepository.save(user);
            }
        });
    }

    @Test
    public void saveUserWithInvalidUsername() {

        user.setUsername("");
        assertThrows(javax.validation.ConstraintViolationException.class, new Executable() {

            @Override
            public void execute() throws Throwable {
                normalUserRepository.save(user);
            }
        });

        user.setUsername(null);
        assertThrows(javax.validation.ConstraintViolationException.class, new Executable() {

            @Override
            public void execute() throws Throwable {
                normalUserRepository.save(user);
            }
        });
    }

    @Test
    public void saveUserWithInvalidPassword() {

        user.setPassword("");
        assertThrows(javax.validation.ConstraintViolationException.class, new Executable() {

            @Override
            public void execute() throws Throwable {
                normalUserRepository.save(user);
            }
        });

        user.setPassword(null);
        assertThrows(javax.validation.ConstraintViolationException.class, new Executable() {

            @Override
            public void execute() throws Throwable {
                normalUserRepository.save(user);
            }
        });
    }

    @Test
    public void saveUserWithInvalidFields() {

        user.setFirstName("");
        user.setLastName("");
        user.setUsername("");
        user.setPassword("");
        assertThrows(javax.validation.ConstraintViolationException.class, new Executable() {

            @Override
            public void execute() throws Throwable {
                normalUserRepository.save(user);
            }
        });

        user.setFirstName(null);
        user.setLastName(null);
        user.setUsername(null);
        user.setPassword(null);
        assertThrows(javax.validation.ConstraintViolationException.class, new Executable() {

            @Override
            public void execute() throws Throwable {
                normalUserRepository.save(user);
            }
        });
    }

}