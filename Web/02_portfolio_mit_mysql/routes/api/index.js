const express = require("express");
const db = require("../../db");

const router = new express.Router();

/**
 * Gets all requests in the database
 */
router.get("/requests", async (req, res) => {
	const result = await db.getAllRequests();
	res.json(result);
});

/**
 * Gets a single request in the database by the id
 */
router.get("/request/:id", async (req, res) => {
	const result = await db.getRequest(req.params.id);
	res.json(result);
});

/**
 * Adds a request to the database
 */
router.post("/request", async (req, res) => {
	const result = await db.addRequest(req.body);
	res.json(result);
});

/**
 * Update a request in the database
 */
router.put("/request/:id", async (req, res) => {
	const result = await db.updateRequest(req.params.id, req.body);
	res.json(result);
});

/**
 * Delete a request from the database
 */
router.delete("/request/:id", async (req, res) => {
	const result = await db.deleteRequest(req.params.id);
	res.json(result);
});

module.exports = router;
