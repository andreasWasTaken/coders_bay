package org.codersbay.fahrzeugabstrakt.vehicle;

import java.util.Calendar;

public class PKW extends AbstractVehicle {

    /**
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * Fields
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */
    private int yearDemoStart;
    private int yearDemoEnd;

    /**
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * Constructor(s)
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */

    /**
     * Main constructor for this PKW
     * 
     * @param id
     * @param brand
     * @param price
     * @param yearOfConstruction
     * @param yearDemoStart      the year this PKW started being used for
     *                           demonstration purposes
     * @param yearDemoEnd        the year this PKW ended being used for
     *                           demonstration purposes
     */
    public PKW(String id, String brand, double price, int yearOfConstruction, int yearDemoStart, int yearDemoEnd) {
        super(id, brand, price, yearOfConstruction);

        setYearDemoStart(yearDemoStart);
        setYearDemoEnd(yearDemoEnd);
    }

    /**
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * Implemented Abstract Methods
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */
    /**
     * Calculates the discount of this PKW depending on the
     * {@code yearOfConstruction} and the period
     * {@code yearDemoStart <= n <= yearDemoEnd} this PKW was used for demonstration
     * purposes
     * 
     * @return The calculated discount for this PKW. The highest possible discount
     *         for a PKW is 10%.
     */
    @Override
    public double getDiscount() {
        Calendar c = Calendar.getInstance();
        int totalYears = c.get(Calendar.YEAR) - yearOfConstruction;
        int totalYearsDemo = yearDemoEnd - yearDemoStart;
        double discount;

        discount = 0.05 * totalYears + 0.03 * totalYearsDemo;
        if (discount >= 0.1) {
            discount = 0.1;
        }
        return discount;
    }

    /**
     * 
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        return sb.append("PKW ").append("(").append(this.id).append(") ").append(this.brand).append(", Baujahr: ")
                .append(this.yearOfConstruction).toString();
    }

    /**
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * Getters and Setters
     * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     */
    /**
     * 
     */
    public int getYearDemoStart() {
        return yearDemoStart;
    }

    /**
     * 
     */
    public void setYearDemoStart(int yearDemoStart) {
        this.yearDemoStart = yearDemoStart;
    }

    /**
     * 
     */
    public int getYearDemoEnd() {
        return yearDemoEnd;
    }

    /**
     * 
     */
    public void setYearDemoEnd(int yearDemoEnd) {
        this.yearDemoEnd = yearDemoEnd;
    }

}
