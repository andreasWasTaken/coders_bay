package com.travelus.backend.controllers;

import com.travelus.backend.models.Destination;
import com.travelus.backend.services.DestinationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/destination")
public class DestinationController {

    /** ****************************************************************************************************************
     * Fields
     ** ****************************************************************************************************************
     */
    @Autowired
    DestinationService destinationService;

    /** ****************************************************************************************************************
     * Methods
     ** ****************************************************************************************************************
     */
    @PostMapping("/{tripId}")
    public ResponseEntity<?> createDestinationAndAddToTrip(@PathVariable Long tripId, @Valid @RequestBody Destination destination,
                                                           BindingResult result) {

        if (result.hasFieldErrors()) {

            return new ResponseEntity<>(result.getFieldErrors(), HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(destinationService.saveOrUpdateTrip(destination, tripId), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteDestinationById(@PathVariable Long id) {

        destinationService.deleteDestinationById(id);

        return new ResponseEntity<>("Destination with ID " + id + " successfully deleted", HttpStatus.OK);
    }

    @GetMapping("/all/{tripId}")
    public Iterable<Destination> getAllDestinationsByTripId(@PathVariable Long tripId) {

        return destinationService.findAllDestinationsByTripId(tripId);
    }

}
