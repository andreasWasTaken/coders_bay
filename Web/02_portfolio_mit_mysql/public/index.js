/** Functions */
function handleResize() {
	let isLargeScreen = window.innerWidth > largeScreen ? true : false;

	if (isLargeScreen) {
		// show navbar for large screens
		$navLarge.classList.remove("d-none");
		$navLarge.classList.add("d-flex");
		$navSmall.classList.remove("d-flex");
		$navSmall.classList.add("d-none");

		// show cards for large screens
		$cardsLarge.classList.remove("d-none");
		$cardsLarge.classList.add("d-flex");
		$cardsSmall.classList.remove("d-flex");
		$cardsSmall.classList.add("d-none");
	} else {
		// show navbar for small screens
		$navLarge.classList.remove("d-flex");
		$navLarge.classList.add("d-none");
		$navSmall.classList.remove("d-none");
		$navSmall.classList.add("d-flex");

		// show cards for small screens
		$cardsSmall.classList.remove("d-none");
		$cardsSmall.classList.add("d-flex");
		$cardsLarge.classList.remove("d-flex");
		$cardsLarge.classList.add("d-none");
	}

	closeNav();
}

function openNav() {
	$navCollapse.style.transform = "translateX(0)";
	document.documentElement.style.overflow = "hidden";
}

function closeNav() {
	$navCollapse.style.transform = "translateX(100%)";
	document.documentElement.style.overflow = "initial";
}

/** Variables */
var largeScreen = 1000;

/** HTML-Elements */
const $navLarge = document.getElementById("nav-large");
const $navSmall = document.getElementById("nav-small");
const $navCollapse = document.getElementById("nav-collapse");
const $cardsLarge = document.getElementById("cards-large");
const $cardsSmall = document.getElementById("cards-small");

/** Event-Listeners */
window.addEventListener("resize", handleResize);

/** Call this function(s) when website loads */
handleResize();
closeNav();
