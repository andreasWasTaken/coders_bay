package com.travelus.backend.repositories;

import com.travelus.backend.models.Destination;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DestinationRepository extends CrudRepository<Destination, Long> {

    Destination findDestinationById(Long id);

    @Override
    Iterable<Destination> findAll();

    @Query("SELECT d FROM Destination d WHERE d.trip.id = ?1")
    Iterable<Destination> findAllDestinationsByTripId(Long tripId);
}
