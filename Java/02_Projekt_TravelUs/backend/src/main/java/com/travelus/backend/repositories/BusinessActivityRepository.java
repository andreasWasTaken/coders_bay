package com.travelus.backend.repositories;

import com.travelus.backend.models.BusinessActivity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BusinessActivityRepository extends CrudRepository<BusinessActivity, Long> {

    BusinessActivity findBusinessActivityById(Long id);

    @Override
    Iterable<BusinessActivity> findAll();

    @Query("SELECT b FROM BusinessActivity b WHERE b.destination.id = ?1")
    Iterable<BusinessActivity> findAllByDestinationId(Long id);
}
