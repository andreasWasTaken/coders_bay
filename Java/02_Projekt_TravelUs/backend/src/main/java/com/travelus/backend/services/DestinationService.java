package com.travelus.backend.services;

import com.travelus.backend.exceptions.IdNotFoundException;
import com.travelus.backend.models.Destination;
import com.travelus.backend.models.Trip;
import com.travelus.backend.repositories.DestinationRepository;
import com.travelus.backend.repositories.TripRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DestinationService {

    /** ****************************************************************************************************************
     * Fields
     ** ****************************************************************************************************************
     */
    @Autowired
    DestinationRepository destinationRepository;

    @Autowired
    TripRepository tripRepository;

    /** ****************************************************************************************************************
     * Methods
     ** ****************************************************************************************************************
     */
    public Destination saveOrUpdateTrip(Destination destination, Long tripId) {

        Trip trip = tripRepository.findTripById(tripId);
        if (trip == null) {
            throw new IdNotFoundException("Cannot create destination. Trip with ID " + tripId + " does not exist.");
        }

        destination.setTrip(trip);
        trip.getDestinations().add(destination);

        return destinationRepository.save(destination);
    }

    public void deleteDestinationById(Long id) {

        if (destinationRepository.findDestinationById(id) == null) {
            throw new IdNotFoundException("Cannot delete destination with ID " + id + ". This destination does not exist");
        }

        Destination destination = destinationRepository.findDestinationById(id);
        Trip trip = tripRepository.findTripById(destination.getTrip().getId());

        trip.getDestinations().remove(destination);
        destinationRepository.deleteById(id);

    }

    public Iterable<Destination> findAllDestinationsByTripId(Long tripId) {

        if (tripRepository.findTripById(tripId) == null) {
            throw new IdNotFoundException("Cannot find destinations of trip with ID " + tripId + ". This trip does not exist.");
        }

        return destinationRepository.findAllDestinationsByTripId(tripId);
    }
}
