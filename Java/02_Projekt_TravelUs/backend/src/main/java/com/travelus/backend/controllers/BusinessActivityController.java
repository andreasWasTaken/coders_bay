package com.travelus.backend.controllers;

import com.travelus.backend.models.BusinessActivity;
import com.travelus.backend.models.CasualActivity;
import com.travelus.backend.services.BusinessActivityService;
import com.travelus.backend.services.CasualActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/b_activity")
@CrossOrigin
public class BusinessActivityController {

    /** ****************************************************************************************************************
     * Fields
     ** ****************************************************************************************************************
     */
    @Autowired
    BusinessActivityService businessActivityService;

    /** ****************************************************************************************************************
     * Methods
     ** ****************************************************************************************************************
     */
    @PostMapping("/{destinationId}")
    public ResponseEntity<?> createActivityAndAddToDestination(@PathVariable Long destinationId,
                                                               @Valid @RequestBody BusinessActivity activity,
                                                               BindingResult result) {
        System.out.println("controller...");
        if (result.hasFieldErrors()) {
            System.out.println("time start: " + activity.getTimeStart());
            System.out.println("time end: " + activity.getTimeEnd());
            System.out.println("has errors1");
            return new ResponseEntity<>(result.getFieldErrors(), HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(businessActivityService.saveOrUpdateActivity(activity, destinationId), HttpStatus.OK);
    }
}
