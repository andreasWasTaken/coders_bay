package com.travelus.backend.payloads;

public class JwtLoginSuccessResponse {

    /** ****************************************************************************************************************
     * Fields
     ** ****************************************************************************************************************
     */
    private boolean success;
    private String token;

    /** ****************************************************************************************************************
     * Constructor
     ** ****************************************************************************************************************
     */
    public JwtLoginSuccessResponse(boolean success, String token) {
        this.success = success;
        this.token = token;
    }

    /** ****************************************************************************************************************
     * Methods
     ** ****************************************************************************************************************
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        return sb.append("JwtLoginSuccessResponse{").append("\n")
                .append("\t").append("success=").append(success).append(",\n")
                .append("\t").append("token='").append(token).append('\'').append("\n")
                .append('}')
                .toString();
    }

    /** ****************************************************************************************************************
     * Getters and Setters
     ** ****************************************************************************************************************
     */
    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
