package com.travelus.backend.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Destination {

    /** ****************************************************************************************************************
     * Fields
     ** ****************************************************************************************************************
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "Attribute latitude must not be null")
    private Double latitude;

    @NotNull(message = "Attribute longitude must not be null")
    private Double longitude;

    @NotBlank(message = "Attribute country must not be an empty string")
    @NotNull(message = "Attribute country must not be null")
    private String country;

    @NotBlank(message = "Attribute locality must not be an empty string")
    @NotNull(message = "Attribute locality must not be null")
    private String locality;

    @NotBlank(message = "Attribute address must not be an empty string")
    @NotNull(message = "Attribute address must not be null")
    private String address;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "tripId", nullable = false)
    @JsonIgnore
    private Trip trip;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "destination")
    private Set<CasualActivity> casualActivities = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "destination")
    private Set<BusinessActivity> businessActivities = new HashSet<>();

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd@HH:MM:SS")
    private Date createdAt;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd@HH:MM:SS")
    private Date updatedAt;

    /** ****************************************************************************************************************
     * Constructor
     ** ****************************************************************************************************************
     */
    public Destination() {
    }

    /** ****************************************************************************************************************
     * Methods
     ** ****************************************************************************************************************
     */
    @PrePersist
    public void onCreate() {
        this.createdAt = new Date();
    }

    @PreUpdate
    public void onUpdate() {
        this.updatedAt = new Date();
    }

    /** ****************************************************************************************************************
     * Getters and Setters
     ** ****************************************************************************************************************
     */
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }

    public Set<CasualActivity> getCasualActivities() {
        return casualActivities;
    }

    public void setCasualActivities(Set<CasualActivity> casualActivities) {
        this.casualActivities = casualActivities;
    }

    public Set<BusinessActivity> getBusinessActivities() {
        return businessActivities;
    }

    public void setBusinessActivities(Set<BusinessActivity> businessActivities) {
        this.businessActivities = businessActivities;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
