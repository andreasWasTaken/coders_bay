package com.travelus.backend.repositories;


import com.travelus.backend.models.NormalUser;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NormalUserRepository extends CrudRepository<NormalUser, Long> {

    NormalUser findNormalUserById(Long id);

    Iterable<NormalUser> findAll();

    @Query("SELECT u FROM NormalUser u WHERE u.isPremium=FALSE")
    Iterable<NormalUser> findAllByPremiumFalse();

    @Query("SELECT u FROM NormalUser u WHERE u.isPremium=TRUE")
    Iterable<NormalUser> findAllByPremiumTrue();

    @Query("SELECT u FROM NormalUser u WHERE u.username=?1")
    NormalUser findNormalUserByUsername(String username);

    NormalUser getById(Long id);
}
