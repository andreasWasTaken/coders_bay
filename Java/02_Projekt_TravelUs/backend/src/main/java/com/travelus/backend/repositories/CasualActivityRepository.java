package com.travelus.backend.repositories;

import com.travelus.backend.models.CasualActivity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CasualActivityRepository extends CrudRepository<CasualActivity, Long> {

    CasualActivity findCasualActivityById(Long id);

    @Override
    Iterable<CasualActivity> findAll();

    @Query("SELECT a FROM CasualActivity a WHERE a.destination.id = ?1")
    Iterable<CasualActivity> findAllByDestinationId(Long id);
}
