package com.travelus.backend.services;

import com.travelus.backend.models.NormalUser;
import com.travelus.backend.repositories.NormalUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CustomNormalUserDetailsService implements UserDetailsService {

    /** ****************************************************************************************************************
     * Fields
     ** ****************************************************************************************************************
     */
    @Autowired
    private NormalUserRepository normalUserRepository;

    /** ****************************************************************************************************************
     * Methods
     ** ****************************************************************************************************************
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        NormalUser user = normalUserRepository.findNormalUserByUsername(username);

        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }

        return user;
    }

    @Transactional
    public NormalUser loadUserById(Long id) {

        NormalUser user = normalUserRepository.getById(id);

        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }

        return user;
    }

}
