package org.codersbay.fahrzeugabstrakt.main;

import java.util.Iterator;
import java.util.Map;

import org.codersbay.fahrzeugabstrakt.infrastructure.CarDealer;
import org.codersbay.fahrzeugabstrakt.vehicle.AbstractVehicle;
import org.codersbay.fahrzeugabstrakt.vehicle.PKW;

public class Main {

    public static void main(String[] args) {

        CarDealer cd = new CarDealer("Autohaus", 3);

        /** Autos ins Autohaus hinzufügen */
        System.out.println("Autos im Autohaus --> " + cd.getNumberOfCars());

        System.out.println("\nAutos hinzufügen ...");
        cd.setVehicle("pkw", "AB-123", "Mercedes", 50000, 2020);
        cd.setVehicle("lkw", "CD-234", "Volvo", 100000, 2020);
        cd.setVehicle("EF-345", "Audi", 60000, 2019, 2020, 2021);
        cd.setVehicle("GH-123", "Mazda", 45000, 2020, 2020, 2021);
        cd.setVehicle("lkw", "IJ-234", "Volvo", 125000, 2020);
        cd.setVehicle("KL-345", "VW", 25000, 2019, 2020, 2021);
        System.out.println("Autos im Autohous --> " + cd.getNumberOfCars());

        /** Autos abfragen */
        System.out.println("\nAutos abfragen ...");
        System.out.println("Auto mit ID 'EF-345' --> " + cd.getVehicle("EF-345"));
        System.out.println("Auto mit ID 'CD-234' --> " + cd.getVehicle("CD-234"));
        System.out.println("Auto mit ID 'KL-345' --> " + cd.getVehicle("KL-345"));

        /** Durch Autos iterieren */
        System.out.println("\nDurch Autos iterieren (mit Iterator) ...");
        Iterator<AbstractVehicle> it = cd.iterator();
        while (it.hasNext()) {
            AbstractVehicle vehicle = it.next();

            if (it.hasNext()) {
                System.out.print(vehicle.getBrand() + ", ");
            } else {
                System.out.print(vehicle.getBrand());
            }
        }

        System.out.println("\n\nDurch Autos iterieren (mit for-each-Schleife) ...");
        int count = 1;
        for (AbstractVehicle vehicle : cd.getVehicles().values()) {
            System.out.print(vehicle.getBrand());

            if (count < cd.getNumberOfCars()) {
                System.out.print(", ");
            }
            count++;
        }

        /** Autohaus überlastet? */
        System.out.println("\n\nParkplätze: " + cd.getNumberOfParkingLots() + ", Autos: " + cd.getNumberOfCars()
                + ", überlastet: " + cd.tooManyCars());
        System.out.println(
                "Angestellte: " + 10 + ", Autos: " + cd.getNumberOfCars() + ", überlastet: " + cd.tooManyCars(10));

        /** Preise der Autos */
        System.out.println("\nPreise der Autos ...");
        for (Map.Entry<String, AbstractVehicle> vehicle : cd.getVehicles().entrySet()) {
            String key = vehicle.getKey();
            AbstractVehicle value = vehicle.getValue();

            System.out.println("Fahrzeug mit ID von " + key + " kostet (" + value.getDiscount() * 100 + "% Rabatt) "
                    + value.getDiscountedPrice() + "€");
        }

        /** LKW oder PKW */
        System.out.println("\nLKW oder PKW ...");
        for (AbstractVehicle vehicle : cd.getVehicles().values()) {
            if (vehicle instanceof PKW) {
                System.out.println("Auto mit ID von " + vehicle.getId() + " ist ein PKW");
            } else {
                System.out.println("Auto mit ID von " + vehicle.getId() + " ist ein LKW");
            }
        }

    }
}
