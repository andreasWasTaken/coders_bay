const JwtStrategy = require("passport-jwt").Strategy;
const JwtExtractor = require("passport-jwt").ExtractJwt;
const JwtService = require("../services/jwtService");

const strategyOptions = {
	jwtFromRequest: JwtExtractor.fromAuthHeaderAsBearerToken(),
	secretOrKey: JwtService.JWT_SECRET
};

const strategy = new JwtStrategy(strategyOptions, (jwt, done) => {
	const admin = { adminId: jwt.sub, name: jwt.name, email: jwt.email };
	done(null, admin);
});

module.exports = strategy;
