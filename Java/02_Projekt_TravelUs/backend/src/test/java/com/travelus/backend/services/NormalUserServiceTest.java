package com.travelus.backend.services;

import com.travelus.backend.models.NormalUser;
import com.travelus.backend.repositories.NormalUserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class NormalUserServiceTest {

    /** ****************************************************************************************************************
     * Fields
     ** ****************************************************************************************************************
     */
    @Autowired
    private NormalUserRepository normalUserRepository;

    @Autowired
    private NormalUserService normalUserService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    private NormalUser user;
    private NormalUser existingUser;

    /** ****************************************************************************************************************
     * Before / After
     ** ****************************************************************************************************************
     */
    @BeforeEach
    void setUp() {

        normalUserRepository.deleteAll();

        if (normalUserRepository.findNormalUserByUsername("test@mail.com") == null) {
            user = new NormalUser();
            user.setFirstName("Test_FirstName");
            user.setLastName("Test_LastName");
            user.setUsername("test@mail.com");
            user.setPassword("Test_Password");
            user.setConfirmPassword("Test_Password");
        }

        if (normalUserRepository.findNormalUserByUsername("existing@mail.com") == null) {
            existingUser = new NormalUser();
            existingUser.setFirstName("Existing_FirstName");
            existingUser.setLastName("Existing_LastName");
            existingUser.setUsername("existing@mail.com");
            existingUser.setPassword("Existing_Password");
            existingUser.setConfirmPassword("Existing_Password");
            normalUserRepository.save(existingUser);
        }
    }

    @AfterEach
    void tearDown() {
    }

    /** ****************************************************************************************************************
     * Tests
     ** ****************************************************************************************************************
     */
    @Test
    void saveValidNormalUserWhoDoesNotExistInDatabase() {

        NormalUser userResult = normalUserService.saveNormalUser(user);
        String encryptedPassword = bCryptPasswordEncoder.encode(user.getPassword());

        assertNotNull(userResult);
        assertTrue(bCryptPasswordEncoder.matches(user.getPassword(), encryptedPassword));
        assertEquals(false, userResult.getPremium());
        assertEquals("", userResult.getConfirmPassword());
    }

    @Test
    void saveValidNormalUserWhoDoesExistInDatabase() {

        // should throw an error
        normalUserService.saveNormalUser(existingUser);
        normalUserService.saveNormalUser(existingUser);
    }
}