package com.travelus.backend.exceptions;

public class UsernameAlreadyExistsExceptionResponse {

    /** ****************************************************************************************************************
     * Fields
     ** ****************************************************************************************************************
     */
    private String email;

    /** ****************************************************************************************************************
     * Constructor
     ** ****************************************************************************************************************
     */
    public UsernameAlreadyExistsExceptionResponse(String email) {
        this.email = email;
    }

    /** ****************************************************************************************************************
     * Getters and Setters
     ** ****************************************************************************************************************
     */
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
