package com.travelus.backend.models;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
public class BusinessActivity extends AbstractActivity {

    /** ****************************************************************************************************************
     * Fields
     ** ****************************************************************************************************************
     */
    @NotNull(message = "Attribute timeStart must not be null")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss")
    private Date timeStart;

    @NotNull(message = "Attribute timeEnd must not be null")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss")
    private Date timeEnd;

    /** ****************************************************************************************************************
     * Constructor
     ** ****************************************************************************************************************
     */
    public BusinessActivity() {
       super();
    }

    /** ****************************************************************************************************************
     * Getters and Setters
     ** ****************************************************************************************************************
     */
    public Date getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(Date timeStart) {
        this.timeStart = timeStart;
    }

    public Date getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(Date timeEnd) {
        this.timeEnd = timeEnd;
    }
}
