const mysql = require("mysql2/promise");
const bcrypt = require("bcryptjs");

let mysqlConnection;
mysql
	.createConnection({
		host: process.env.DB_HOST,
		user: process.env.DB_USER,
		database: process.env.DB_DATABASE,
		password: ""
	})
	.then((con) => {
		mysqlConnection = con;
		console.log("Connected to MySQL database 'andreas_was_taken'");
	})
	.catch((err) => console.error(err));

async function getAllRequests() {
	const [result] = await mysqlConnection.query("SELECT * FROM requests");
	return result;
}

async function getRequest(id) {
	const [result] = await mysqlConnection.query("SELECT * FROM requests WHERE id=?", [id]);
	return result;
}

async function addRequest(request) {
	const [result] = await mysqlConnection.query("INSERT INTO requests (name, email, text) VALUES (?, ?, ?)", [
		request.name,
		request.email,
		request.text
	]);
	return request;
}

async function updateRequest(id, update) {
	const [result] = await mysqlConnection.query("UPDATE requests SET name=?, email=?, text=? WHERE id=?", [
		update.name,
		update.email,
		update.text,
		id
	]);
	return result;
}

async function deleteRequest(id) {
	const [result] = await mysqlConnection.query("DELETE FROM requests WHERE id=?", [id]);
	return result;
}

async function createAdmin(admin) {
	const [adminFound] = await mysqlConnection.query("SELECT * FROM admins WHERE email=?", [admin.email]);

	if (adminFound.length > 0) {
		throw new Error("Admin with email '" + admin.email + "' already exists");
	}

	const salt = await bcrypt.genSalt(10);
	const hashPassword = await bcrypt.hash(admin.password, salt);

	const [result] = await mysqlConnection.query(
		"INSERT INTO admins (firstName, lastName, email, password) VALUES (?, ?, ?, ?)",
		[admin.firstName, admin.lastName, admin.email, hashPassword]
	);

	return result;
}

async function getAdminById(id) {
	const [result] = await mysqlConnection.query("SELECT * FROM admins WHERE id=?", [id]);
	return result;
}

async function getAdminByEmail(email) {
	const [result] = await mysqlConnection.query("SELECT * FROM admins WHERE email=?", [email]);
	return result;
}

async function getAllAdmins() {
	const [result] = await mysqlConnection.query("SELECT * FROM admins");
	return result;
}

async function deleteAdminById(id) {
	const [result] = await mysqlConnection.query("DELETE FROM admins WHERE id=?", [id]);
	return result;
}

async function deleteAdminByEmail(email) {
	const [result] = await mysqlConnection.query("DELETE FROM admins WHERE email=?", [email]);
	if (result.affectedRows === 0) {
		throw new Error("Cannot delete admin with email '" + email + "'. Admin does not exist");
	}
	return result;
}

async function findAdminByCredentials(email, password) {
	const [result] = await mysqlConnection.query("SELECT * FROM admins WHERE email=? LIMIT 1", [email]);
	if (!result.length) {
		throw new Error("No admin with given email found");
	}
	const admin = result[0];

	const doPasswordsMatch = await bcrypt.compare(password, admin.password);
	if (!doPasswordsMatch) {
		throw new Error("Passwords do not match");
	}

	delete admin.password;

	return admin;
}

async function doesEmailExist(email) {
	const [result] = await mysqlConnection.query("SELECT * FROM admins WHERE email=?", [email]);

	if (!result.length) {
		return false;
	}

	return true;
}

module.exports = {
	getAllRequests,
	getRequest,
	addRequest,
	updateRequest,
	deleteRequest,
	createAdmin,
	getAdminById,
	getAdminByEmail,
	getAllAdmins,
	deleteAdminById,
	deleteAdminByEmail,
	findAdminByCredentials,
	doesEmailExist
};
