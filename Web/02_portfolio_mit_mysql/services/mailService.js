const nodeMailer = require("nodemailer");

const mailTransport = nodeMailer.createTransport({
	service: process.env.EMAIL_SERVICE,
	host: process.env.EMAIL_HOST,
	port: 465,
	secure: true,
	auth: {
		user: process.env.EMAIL_USER,
		pass: process.env.EMAIL_PASSWORD
	}
});

function sendMailToUser(to, subject, text) {
	const options = {
		from: process.env.EMAIL,
		to,
		subject,
		text
	};

	mailTransport.sendMail(options, (error, info) => {
		if (error) {
			return error;
		} else {
			return info;
		}
	});
}

module.exports = sendMailToUser;
